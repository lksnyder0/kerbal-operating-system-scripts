# WUT
Flight control and automation scripts for Kerbal Operating System. A plugin for Kerbal Space Program.

## Log levels
| level number | level name |
| ------------ | ---------- |
| 10           | FATAL      |
| 20           | ERROR      |
| 30           | WARNING    |
| 40           | IMPORTANT  |
| 50           | INFO       |
| 60           | DEBUG      |