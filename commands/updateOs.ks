
function copyOrCompileLib {
    parameter name, 
              comp is True.
    
    if not(EXISTS("libs")) createDir("libs").

    local srcPath is "0:/libs/" + name + ".ks".
    if exists(srcPath) {
        if comp {
            local dstPath is "libs/" + name + ".ksm".
            if exists(console_log) console_log(INFO, "Compiling " + srcPath + " to " + dstPath).
            compile srcPath to dstPath.
        } else {
            local dstPath is "libs/" + name + ".ks".
            if exists(console_log) console_log(INFO, "Copying " + srcPath + " to " + dstPath).
            copyPath(srcPath, dstPath).
        }
    } else {
        if exists(console_log) console_log(ERROR, "Can't find library " + name + "!").
        else print "[ERROR] Can't find library " + name + "!".
    }
}

function copyCommand {
    parameter name.

    if exists(name) {
        copyPath("0:/commands/" + name + ".ks", "").
    } else {
        console_log(ERROR, "Can't find command " + name + "!").
    }
}

// Copy libraries
copyOrCompileLib("logging", false).
runOncePath("libs/logging").

console_log(ALWAYS, "Updating OS").

local comp is log_level <> DEBUG.
copyOrCompileLib("base", comp).
copyOrCompileLib("orbit", comp).
copyOrCompileLib("launch", comp).
copyOrCompileLib("parts", comp).

copyPath("0:/commands/doNextManeuver.ks", "").
copyPath("0:/commands/changeOrbit.ks", "").
copyPath("0:/commands/circularize.ks", "").
copyPath("0:/commands/updateOs.ks", "").
copyPath("0:/changeLogLevel.ks", "").
