parameter doat, warp is false.

runOncePath("libs/orbit").
runOncePath("libs/base").

local mnv_vector is find_circularize_vector(doat).
add_maneuver(mnv_vector).
do_next_maneuver(warp, true, true).
