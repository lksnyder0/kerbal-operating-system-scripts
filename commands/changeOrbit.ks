parameter doat, 
          oparam, 
          dval, 
          warp is false, 
          autoStage is false,
          scoreMnv is false.

runOncePath("libs/orbit").
runOncePath("libs/base").

local mnvVector is findOrbitChangeVector(doat, oparam, dval).
add_maneuver(mnvVector).
do_next_maneuver(warp, autoStage, scoreMnv).
