// Open the terminal
core:part:getmodule("kOSProcessor"):doevent("Open Terminal").

runPath("0:/commands/updateOs.ks").
// Copy ship specific script
local safeShipName is shipName:replace(" ", "_"):tolower.
local shipscript is "0:/ships/" + safeShipName + ".ks".
if exists(shipscript) {
    copyPath(shipscript, "").
    runPath(safeShipName + ".ks").
} else {
    console_log(ERROR, "Can't find ship specific script. Looking for " + shipscript).
}
