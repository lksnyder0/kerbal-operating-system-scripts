
// Guess we are on a launch pad.
if alt:radar < 500 and ship:body:name = "kerbin" {
    // Open the terminal
    core:part:getmodule("kOSProcessor"):doevent("Open Terminal").
    // Copy launch script locally
    copyPath("0:/launch.ks", "").
    // Call launch script to send craft to 100km at due east
    runPath("launch.ks", 80000, 90).
}