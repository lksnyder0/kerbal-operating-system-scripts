// Always bring logging
runOncePath("libs/base").
runOncePath("libs/orbit").
runOncePath("libs/logging").

set_log_level(DEBUG).
ensure_engines_active().
countdown_timer(5).
local new_mnv is findOrbitChangeVector("apoapsis", "apoapsis", 300000).
print new_mnv.
add_maneuver(new_mnv).
do_next_maneuver(true, true, true).
countdown_timer(5).
set new_mnv to findOrbitChangeVector("apoapsis", "periapsis", 150000).
add_maneuver(new_mnv).
do_next_maneuver(true, true, true).
countdown_timer(5).
set new_mnv to find_circularize_vector("periapsis").
add_maneuver(new_mnv).
do_next_maneuver(true, true, true).
