// Always bring logging
runOncePath("libs/launch").
runOncePath("libs/logging").

set_log_level(DEBUG).

if alt:radar < 500 and body:name = "Kerbin" {
    do_launch(80000, 90, 2).
}