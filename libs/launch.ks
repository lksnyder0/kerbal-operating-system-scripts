runOncePath("libs/base").
runOncePath("libs/logging").
runOncePath("libs/orbit").

global throt is 0.

// Functional Launch Script
function do_launch {
  parameter dalt, shead, circularizationStage is false.
  global desired_altitude is dalt.
  global starting_heading is shead.
  // Override default throttle settings
  set SHIP:CONTROL:PILOTMAINTHROTTLE to 0.
  console_log(INFO, "Sending craft to " + (desired_altitude/1000) + "km orbit with a heading of " + starting_heading + ".").
  lock throttle to throt.
  countdown_timer(10).
  start_launch().
  do_ascent().
  until apoapsis >= desired_altitude {
    do_auto_stage().
    wait 1.
  }
  set throt to 0.
  wait 1.
  if circularizationStage < stage:number {
    console_log(DEBUG, "Staging to circularization stage.").
    until circularizationStage = stage:number {
      do_safe_stage().
    }
  }
  console_log(INFO, "Coasting out of atmosphere.").
  // Until the ship is out of the atmosphere
  until alt:radar > 70000 {
    do_coast().
    wait 0.
  }

  // Enable debug logging
  local oldLogLevel is log_level.
  local circularVector is find_circularize_vector("apoapsis").
  add_maneuver(circularVector).
  do_next_maneuver(false, false, true).
  console_log(INFO, "In orbit!").
  console_log(
    DEBUG, 
    "Acheived orbit:
Apoapsis: " + ship:orbit:apoapsis + "
Perapsis: " + ship:orbit:periapsis + "
Inclination: " + ship:orbit:inclination + "
Eccentricity: " + ship:orbit:eccentricity
    ).
    unlock steering.
    unlock throttle.
}

function do_coast {
  lock steering to prograde.
  if apoapsis < desired_altitude - (desired_altitude * 0.002) {
    console_log(DEBUG, "Boosting back to desired altitude.").
    set throt to 0.15.
    wait until apoapsis >= desired_altitude + (desired_altitude * 00.005).
    set throt to 0.
  }
}

function start_launch {
  set throt to 1.
  ensure_engines_active().
  console_log(IMPORTANT, "LIFT OFF!").
}

function do_ascent {
  set targetDirection to starting_heading.
  // Start flying straight up
  set targetPitch to 90.
  lock steering to heading(targetDirection, targetPitch).

  // Once we reach 100m/s or an altitude of 1km, start our turn
  when ship:airspeed > 100 or alt:radar > 1000 then {
    console_log(INFO, "Starting gravity turn.").
    console_log(DEBUG, "airspeed: " + ship:airspeed).
    console_log(DEBUG, "altitude: " + alt:radar).
    lock targetPitch to 88.963 - 1.03287 * alt:radar^0.409511.
  }
}

function do_shutdown {
  console_log(IMPORTANT, "Shutting down engines.").
  lock throttle to 0.
  lock steering to prograde.
}