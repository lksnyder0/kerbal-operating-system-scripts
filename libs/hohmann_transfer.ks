parameter dest_name is "Mun", capture is false.

function find_hohmann_deltav {
  parameter destination_name, capture_burn is false.
  local total_dv is 0.
  local deltav1 is 0.
  local deltav2 is 0.
  local body1 is ship:body.
  local body2 is body(destination_name).
  local mu is 0.
  local r1 is 0.
  local r2 is 0.
  // local angle1 is -1.
  // local angle2 is -1.
  // local angle3 is -1.
  local phase_angle is -1.

  local moon_transfer is target_orbiting_body(body1, body2).

  // Calculate dV estimate
  if moon_transfer {
    print "Mu from: " + body1:name.
    set mu to body1:mu.
    set r1 to ship:orbit:semimajoraxis.
    set r2 to body2:orbit:semimajoraxis.

    // // Ship's angle to universal refrence direction
    // set angle1 to ship:orbit:argumentofperiapsis + ship:orbit:trueanomaly.
    // set angle1 to angle1 - 360 * floor(angle1/360). // Normalization
    // // Target angle
    // set angle2 to body2:orbit:lan + body2:orbit:argumentofperiapsis + body2:orbit:trueanomaly.
    // set angle2 to angle2 - 360 * floor(angle2/360). // Normalization

    // set angle3 to angle2 - angle1.
    // set angle3 to angle3 - 360 * floor(angle3/360).

    // print "Ship's angle: " + angle1.
    // print "Target's angle: " + angle2.
    // print "Angle3: " + angle3.
  } else {
    print "Mu from: Sun".
    set mu to Body("Sun"):mu.
    set r1 to body1:orbit:semimajoraxis.
    set r2 to body2:orbit:semimajoraxis.
  }
  set deltav1 to abs(sqrt(mu/r1) * ( sqrt((2 * r2)/(r1 + r2)) - 1 )).
  set total_dv to total_dv + deltav1.
  print "deltaV 1: " + deltav1.
  if capture_burn {
    set deltav2 to abs(sqrt(mu/r2) * (1 - sqrt((2 * r2) / (r1 + r2)))).
    set total_dv to total_dv + deltav2.
    print "deltaV 2: " + deltav2.
  }
  print "Total deltaV: " + total_dv.

  // Maneuver time estimate
  if moon_transfer {
    set tgt_orbital_period to body2:orbit:period.
    set angle1 to abs(360 * (transfer_time / tgt_orbital_period) - 180).
  } else {
    print "Noop".
  }

}

function target_orbiting_body {
  parameter body1, target.

  if not(body1:ORBITINGCHILDREN:empty) {
    for each in body1:ORBITINGCHILDREN {
      if each = target return true.
    }
  }
  return false.
}

find_hohmann_deltav(dest_name, capture).
