GLOBAL ALWAYS IS 0.
GLOBAL FATAL IS 10.
GLOBAL ERROR IS 20.
GLOBAL WARNING IS 30.
GLOBAL IMPORTANT IS 40.
GLOBAL INFO IS 50.
GLOBAL DEBUG IS 60.

if not(defined log_level) GLOBAL log_level is IMPORTANT.

global function set_log_level {
  parameter level.
  set log_level to level.
  local level_string is get_level_string(level).
  console_log(ALWAYS, "Set level to " + level_string).
}

function get_level_string {
  parameter level.
  
  local level_string is "".
  // Default to warning level if logging is not configured.
  if level = 0 {
    set level_string to "ALWAYS".
  } else if level <= FATAL {
      set level_string to "FATAL".
  } else if level <= ERROR {
      set level_string to "ERROR".
  } else if level <= WARNING {
      set level_string to "WARNING".
  } else if level <= IMPORTANT {
      set level_string to "IMPORTANT".
  } else if level <= INFO {
      set level_string to "INFO".
  } else {
      set level_string to "DEBUG".
  }
  return level_string.
}

global function console_log {
  parameter level, message.
  if level <= log_level or level = 0 {
    local level_string is get_level_string(level).
    print "[" + level_string + "] " + message.
  } 
}
