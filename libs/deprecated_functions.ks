function stageHasDeployEngines {
  local engList is getCurrentStageEngines().
  if engList:length > 0 {
    for en in engList {
      print en:title.
      print en:modules.
      if en:hasModule("ModuleDeployableEngine") {
        return true.
      }
    }
  }
  return false.
}

// Returns true if engine is in current stage
function engineInStage {
  parameter eng.
  return eng:stage = stage:number.
}

function getCurrentStageEngines {
  list engines in myEngines.
  set stageEngines to list().
  for en in myEngines {
    if engineInStage(en) {
      stageEngines:add(en).
    }
  }
  return stageEngines.
}
