runOncePath("libs/logging").

// Get Science parts
function get_science_capable_parts {
  local scienceParts is list().

  list parts in FOO.

  for p in FOO {
    if p:hasmodule("experiment") {
      console_log(DEBUG, "Found experiment module on " + p:title).
      scienceParts:add(p).
    }
  }
  
  return scienceParts.

}

function start_science_on_part {
  parameter part.

  if part:hasModule("experiment") {
    local exp is part:getModule("experiment").
    for act in exp:allActionNames {
      if act:startswith("start:") {
        console_log(DEBUG, "CALLING -> " + act).
        exp:doaction(act, true).
      }
    }
  }
}

// Returns list of active engines
// None -> List
function getNumActiveEngines {
  list engines in myEngines.
  set activeEngines to 0.
  for en in myEngines {
    if en:ignition and not en:flameout {
      set activeEngines to activeEngines + 1.
    }
  }
  return activeEngines.
}

// Get Comms

// Get Solar panels
