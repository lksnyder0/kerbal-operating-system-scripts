// Library to provide elementary functions or values
runOncePath("libs/logging").
runOncePath("libs/parts").

function add_maneuver {
  parameter mList.

  set utime to mList[0].
  set radial_mag to mList[1].
  set normal_mag to mList[2].
  set prograde_mag to mList[3].

  local mnv is node(utime, radial_mag, normal_mag, prograde_mag).

  add mnv.

}

// Execute maneuver with up to 0.1m/s precision
// doWarp:Bool
// scoreManeuver:Bool
// doAutoStage:Bool
function do_next_maneuver {
  parameter doWarp is false,
            doAutoStage is false, 
            scoreManeuver is false.

  if hasNode {
    set mnv to nextNode.
  } else {
    print("No maneuver to execute").
    return.
  }

  local sas_status is sas.
  if sas_status {
    sas off.
  }
  
  ensure_engines_active().

  local expectedOrbit is list(
    mnv:orbit:apoapsis,
    mnv:orbit:periapsis,
    mnv:orbit:inclination,
    mnv:orbit:eccentricity
  ).

  console_log(DEBUG, "Node in: " + round(mnv:eta) + ", DeltaV: " + round(mnv:deltav:mag)).
  set burn_duration to maneuver_burn_time(mnv).
  console_log(DEBUG, "Burn Estimate: " + round(burn_duration)).
  console_log(DEBUG, "Start Burn in: " + (mnv:eta - (burn_duration / 2))).

  // Point ship in burn vector direction
  lock steering to mnv:burnvector.
  
  // Wait until ship is facing the burn direction
  wait until vang(mnv:burnvector, ship:facing:vector) < 0.25.

  if doWarp {
    // Warp to 15 seconds before burn start
    kuniverse:timewarp:warpto(time:seconds + (mnv:eta - (burn_duration/2) - 15) ).
    // Ensure ship is pointing in the correct direction
    lock steering to mnv:burnvector.
  }

  // Wait until it is time to burn
  wait until mnv:eta <= (burn_duration/2).

  // Do the burn
  set tset to 0.
  lock throttle to tset.

  set done to False.
  set dv0 to mnv:deltav.
  until done {
    if doAutoStage {
      do_auto_stage().
    }

    // recalculate current max_acceleration, as it changes while we burn through fuel
    set max_acc to ship:availableThrust/ship:mass.

    // throttle is 100% until there is less than 1 second of time left to burn
    // when there is less than 1 seconds - decrease the throttle linearly
    set tset to min(mnv:deltav:mag/max_acc, 1).
    
    // here's the tricky part, we need to cut the throttle as soon as our nd:deltav and initial deltav start facing opposite directions
    // this check is done via checking the dot product of those 2 vectors
    if vdot(dv0, mnv:deltav) < 0
    {
      lock throttle to 0.
      console_log(WARNING, "Overshot the maneuver by: dv " + round(mnv:deltav:mag,1) + "m/s, vdot: " + round(vdot(dv0, mnv:deltav),1)).
      break.
    }

    // we have very little left to burn, less then 0.1m/s
    if mnv:deltav:mag < 0.1
    {
      console_log(DEBUG, "Finalizing burn, remain dv " + round(mnv:deltav:mag,1) + "m/s, vdot: " + round(vdot(dv0, mnv:deltav),1)).
      //we burn slowly until our node vector starts to drift significantly from initial vector
      //this usually means we are on point
      wait until vdot(dv0, mnv:deltav) < 0.5.

      lock throttle to 0.
      console_log(DEBUG, "End burn, remain dv " + round(mnv:deltav:mag,1) + "m/s, vdot: " + round(vdot(dv0, mnv:deltav),1)).
      set done to True.
    }
  }
  unlock steering.
  unlock throttle.
  if sas_status {
    sas on.
  }
  if scoreManeuver {
    local maneuverScore is score_executed_maneuver(expectedOrbit).
    print "Maneuver score was " + maneuverScore + ". (1 is perfect)".
  }
  wait 0. // Wait one tick

  remove mnv.
}

// Calculate burn time
// mnv:Node -> Scalar
function maneuver_burn_time {
  parameter mnv.
  // Ideal rocket equation
  // dV = v(e) * ln(m0 / mf) 
  // dV   -> delta V or change in velocity
  // v(e) -> Effective exaust velocity
  // m0   -> Initial craft mass
  // mf   -> Final craft mass
  // Or with ISP
  // v(e) = isp * g0
  // dV = isp * g0 * ln(m0 / mf) 
  // isp  -> Specific Impulse
  // g0   -> Standard gravity = 9.80665 m/s
  // mf = m0 - (fuelFlow * t)
  // F = g0 * isp * m(dot)
  // F    -> Thrust
  // m(dot) -> Mass flow rate (fuelFlow)

  // local g0 is 9.80665.
  local m0 is ship:mass.
  local F is ship:availableThrust.
  local dV is mnv:deltaV:mag.
  local isp is 0.

  list engines in myEngines.

  for en in myEngines {
    if en:ignition and not en:flameout {
      set isp to isp + (en:isp * (en:availableThrust / ship:availableThrust)).
    }
  }

  // Solve dV = isp * g0 * ln(m0 / mf) for mf
  // mf = m0 / e^(dV / (isp * g0))
  local mf is m0 / constant():e ^ (dV / (isp * constant:g0)).

  // Solve F = g0 * isp * m(dot) for m(dot) AKA fuelFlow
  local fuelFlow is F / (isp * constant:g0).

  // Solve mf = m0 - (FuelFlow * t) for T
  local t is (m0 - mf) / fuelFlow.
  
  return t.
}

// Calculate burn start time based on starting half total burn time before node ETA
// mnv:Node -> Scalar
function calculate_start_time {
  parameter mnv.
  // Start the maeuver 30 seconds early
  local burnTime is maneuver_burn_time(mnv).
  return time:seconds + (mnv:eta - (burnTime / 2)).
}

// Lock steering to maneuver burnvector
// mnv:Node -> None
function lock_steering_at_maneuver_target {
  parameter mnv.
  lock steering to mnv:burnvector.
}

// Provides countdown timer
// countdown:Scalar -> None
function countdown_timer {
    parameter countdown.
    PRINT "Counting down:".
    UNTIL countdown = 0 {
        PRINT "..." + countdown.
        WAIT 1. // pauses the script here for 1 second.
        SET countdown to countdown - 1.
    }
}

// do_safe_stage until at least one engine is active
// None -> None
function ensure_engines_active {
    set activeEngines to getNumActiveEngines().
    console_log(DEBUG, "Found " + activeEngines + " active engines").
    until activeEngines > 0 {
      console_log(DEBUG, "No engines active. Staging again.").
      do_safe_stage().
      set activeEngines to getNumActiveEngines().
      console_log(DEBUG, "Found " + activeEngines + " active engines").
    }
  wait 0.
}

// Score executed maneuver
// expectedOrbit:List -> Scalar
// expectedOrbit:List(apoapsis, periapsis, inclination, eccentricity)
// The returned score is the percent correct the maneuver was. Calculated by
// 1 - avg(
//         perc_diff(expected_apoapsis, achieved_apoapsis), 
//         perc_diff(expected_periapsis, achieved_periapsis),
//         perc_diff(expected_inclination, achieved_inclination),
//         perc_diff(expected_eccentricity, achieved_eccentricity))
// 1 is a perfect score.
function score_executed_maneuver {
  parameter expectedOrbit.

  local app_perc_diff is percent_diff(expectedOrbit[0], ship:orbit:apoapsis).
  local peri_perc_diff is percent_diff(expectedOrbit[1], ship:orbit:periapsis).
  local incl_perc_diff is percent_diff(expectedOrbit[2], ship:orbit:inclination).
  local ecce_perc_diff is percent_diff(expectedOrbit[3], ship:orbit:eccentricity).
  local maneuverScore is 1 - ((app_perc_diff + peri_perc_diff + incl_perc_diff + ecce_perc_diff) / 4).

  return maneuverScore.
}

function percent_diff {
  parameter val1, val2.
  return abs(val1 - val2) / ((val1 + val2) / 2).
}

function do_safe_stage {
  wait until stage:ready.
  console_log(WARNING, "Staging.").
  stage.
}

function do_auto_stage {
  if not(defined oldThrust) {
    declare global oldThrust to ship:availablethrust.
  }
  if ship:availablethrust < (oldThrust - 10) {
    until false {
      do_safe_stage().
      wait 0.
      if ship:availableThrust > 0 {
        break.
      }
    }
    set oldThrust to ship:availablethrust.
  }
}

function do_reentry_steering {
  set svector to retrograde + R(0, -4, 0).
  until alt:radar < 10000 {
    lock steering to svector.
  }
  lock steering to retrograde.
}
