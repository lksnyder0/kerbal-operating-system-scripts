// Library to provide various orbital maneuvers
runOncePath("libs/logging").

// Find circularization maneuver at provided position
// position:String
// valid values for position are: string:"apoapsis", string:"periapsis", scalar:<ralative seconds>
// Returns list(uTime_to_maneuver, prograde_magnitude, radial_magnitude, normal_magnitude)
function find_circularize_vector {
  parameter position.

  local circ is list(findDoAtTime(position), 0, 0, 0).

  if not(circ:istype("List")) {
    return.
  }

  console_log(INFO, "Starting circularization.").
  set circ to improve_converge(circ, protect_from_past(score_eccentricity@)).
  console_log(DEBUG, "Circular burn vector: " + circ).

  return circ.
}

// Find orbital change vector
// doat:(String|Scalar) Orbit position or time from now to execute burn
// oparam:String Orbit parameter to change
// dval:Scalar Desired value
function findOrbitChangeVector {
  parameter doat, // Do burn at
            oparam, // Parameter to change
            dval. // Desired Value

  if not(list("apoapsis", "periapsis"):contains(doat)) or doat:istype("Scalar") {
    console_log(ERROR, "Invalid doat value!").
    return.
  }
  if not(list("apoapsis", "periapsis"):contains(oparam)) {
    console_log(ERROR, "Invalid parameter to change.").
    return.
  }


  local mnvTime is findDoAtTime(doat).
  local scoreFunc is False.

  if oparam = "apoapsis" {
    set scoreFunc to scoreApoapsis@:bind(mnvTime, dval).
  } else if oparam = "periapsis" {
    set scoreFunc to scorePeriapsis@:bind(mnvTime, dval).
  }

  set dv to improve_converge(list(0), scoreFunc)[0].

  if list("apoapsis", "periapsis"):contains(oparam) {
    set new_mnv to list(mnvTime, 0, 0, dv).
  }
  console_log(DEBUG, "Burn vector: " + new_mnv).

  return new_mnv.
}

function findDoAtTime {
  parameter doat.
  if doat = "apoapsis" {
    return time:seconds + eta:apoapsis.
  } else if doat = "periapsis" {
    return time:seconds + eta:periapsis.
  } else if doat:istype("Scalar") {
    return time:seconds + doat.
  } else {
    console_log(ERROR, "Provided argument is not one of ('apoapsis', 'periapsis', <scalar>),").
    return 0.
  }
}

// Given a seed maneuver, converge in a better maneuver
function improve_converge {
  parameter seed, score_function.

  local maneuver is seed.
  local steps is list(100, 50, 10, 5, 1, .1).
  local guesses is 0.
  local oldScore is 100000.
  local scoreToBeat is 1000000.

  for step in steps {
    until false {
      local maneuver_data to improve(maneuver, scoreToBeat, step, score_function).
      set maneuver to maneuver_data[0].
      set scoreToBeat to maneuver_data[1].
      set guesses to guesses + 1.
      console_log(DEBUG, "oldScore: " + oldScore + " scoreToBeat: " + scoreToBeat).
      if oldScore <= scoreToBeat {
        // Didn't improve! Assume good
        break.
      }
      set oldScore to scoreToBeat.
    }
  }
  console_log(DEBUG, "Maneuver guesses: " + guesses).
  return maneuver.
}

// List -> number (lower is better)
function score_eccentricity {
  parameter data.
  local ecc_weight is 10.
  local inc_weight is 30.

  local mnv is node(data[0], data[1], data[2], data[3]).
  add mnv.
  local eccentricity is mnv:orbit:eccentricity.
  local inc_diff is abs(mnv:orbit:inclination - ship:orbit:inclination).
  local result is ((eccentricity * ecc_weight) + (inc_diff * inc_weight)) / (ecc_weight + inc_weight).
  remove mnv.

  return result.
}

function scoreApoapsis {
  parameter mtime, dval, data.

  local mnv is node(mtime, 0, 0, data[0]).
  add mnv.
  local result is abs(mnv:orbit:apoapsis - dval).
  remove mnv.
  return result.
}

function scorePeriapsis {
  parameter mtime, dval, data.

  local mnv is node(mtime, 0, 0, data[0]).
  add mnv.
  local result is abs(mnv:orbit:periapsis - dval).
  remove mnv.
  return result.
}

// data:List, scoreToBeat:Scalar, step:Scalar -> List
function improve {
  parameter data, scoreToBeat, step, score_function.

  local bestCanditate is data.

  local candidates is list().
  local index is 0.
  until index >= data:length {
    local inc_candidate is data:copy().
    local dec_candidate is data:copy().

    set inc_candidate[index] to inc_candidate[index] + step.
    set dec_candidate[index] to dec_candidate[index] - step.

    candidates:add(inc_candidate).
    candidates:add(dec_candidate).

    set index to index + 1.
  }

  for candidate in candidates {
    local candidateScore is score_function(candidate).
    if candidateScore < scoreToBeat {
      set scoreToBeat to candidateScore.
      set bestCanditate to candidate.
    }
  }

  return list(bestCanditate, scoreToBeat, 4).
}

function protect_from_past {
  parameter orig_func.
  local protect is {
    parameter data.
    if data[0] < time:seconds + 60 {
      return 2^64.
    } else {
      return orig_func(data).
    }
  }.
  return protect@.
}
